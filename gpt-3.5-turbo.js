require('dotenv').config();
const express = require('express');
const { Configuration, OpenAIApi } = require("openai");
const cors = require('cors');
const app = express();
app.use(express.json());
app.use(cors());

const port = process.env.PORT || 3050;

const configuration = new Configuration({
  apiKey: process.env.OPENAI_KEY,
});
const openai = new OpenAIApi(configuration);

app.post("/ask", async (req, res) => {
    const messages = req.body.messages;
    console.log('messages', messages);
    try {
      if (messages == null) {
        throw new Error("Uh oh, no messages was provided");
      }
      const response = await openai.createChatCompletion({
        model: "gpt-3.5-turbo",
        temperature: 0,
        max_tokens: 2096,
        top_p: 1,
        messages,
      });
      const completion = response.data.choices[0].message;
      console.log('completion', completion);
      return res.status(200).json({
        success: true,
        message: completion,
      });
    } catch (error) {
      console.log(error.message);
    }
  });

app.listen(port, () => console.log(`Server is running on port ${port}!!`));
