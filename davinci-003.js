require('dotenv').config();
const express = require('express');
const { Configuration, OpenAIApi } = require("openai");

const app = express();
const cors = require('cors');
app.use(cors());
app.use(express.json());

const port = process.env.PORT || 3050;

const configuration = new Configuration({
  apiKey: process.env.OPENAI_KEY,
});
const openai = new OpenAIApi(configuration);

// Iniciar o servidor
app.get("/ask", async (req, res) => {
    const prompt = req.query.prompt;
    console.log('prompt', prompt);
    try {
      if (prompt == null) {
        throw new Error("Uh oh, no prompt was provided");
      }
      const response = await openai.createCompletion({
        model: "text-davinci-003",
        prompt,
      });
      console.log('response')
      const completion = response.data.choices[0].text;
      return res.status(200).json({
        success: true,
        message: completion,
      });
    } catch (error) {
      console.log(error.message);
    }
  });

  app.listen(port, () => console.log(`Server is running on port ${port}!!`));
